/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exeception;

public class ElementNotFoundException extends Exception {

    /**
     * Excepção se o elemento nao existir, sem mensagem.
     * {@link ElementNotFoundException}
     */
    public ElementNotFoundException() {
    }

     /**
     * Excepção se o elemento nao existir, com mensagem detalhada.
     * {@link ElementNotFoundException} com mensagem detalhada.
     *
     * @param msg mensagem detalhada
     */
    public ElementNotFoundException(String msg) {
        super(msg);
    }
}
