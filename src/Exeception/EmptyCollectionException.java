/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exeception;

/**
 *
 * @author rafaelsantos
 */
public class EmptyCollectionException extends Exception {

    /**
     * Método de instanciação
     * {@link EmptyCollectionException}
     */
    public EmptyCollectionException()    
    {
    }

    

    /**
     * Excepção de collecção vazia com mensagem.
     * {@link EmptyCollectionException} com mensagem.
     *
     * @param message
     */
    public EmptyCollectionException(String message) {
        super(message);
    }
    

   
    
}
