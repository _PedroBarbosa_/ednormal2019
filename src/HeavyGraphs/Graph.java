/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HeavyGraphs;

import Arrays.ArrayStack;
import Arrays.ArrayUnorderedList;
import Exeception.EmptyCollectionException;
import Interfaces.GraphADT;
import Lists.LinkedQueue;
import Lists.StackList;
import Lists.UnorderedLinkedList;
import ed_trabalhopraticoen.Profissional;
import ed_trabalhopraticoen.Utilizador;
import java.util.Iterator;

/**
 *
 * @author rafaelsantos
 * @param <T>
 */
public class Graph<T extends Comparable<? super T>> implements GraphADT<T> {

    protected final int DEFAULT_CAPACITY = 10;
    //Numero de vértices no grafo
    protected int numVertices;
    //Matriz de adjacência
    protected double[][] adjMatrix;
    //Valores dos vértices
    protected T[] vertices;

    //Crie um grafo vazio
    public Graph() {
        numVertices = 0;
        this.adjMatrix = new double[DEFAULT_CAPACITY][DEFAULT_CAPACITY];
        this.vertices = (T[]) (new Comparable[DEFAULT_CAPACITY]);
    }

    /*
     * Retorna uma representação de string da matriz de adjacência auxiliado pelo array de arestas T[] vertices. 
     * Verifica os valores que a matriz adjMatrix contém, caso tenha valor válido imprime o valor, caso o valor 
     * em questão seja 0 ou menor ou seja seja Infinito, é imprimido "-". 
     *
     * @return result Retorna uma string completa com os valores da adjMatrix formatada de forma a ser facilmente
     * legivel.
     */
    @Override
    public String toString() {
        if (numVertices == 0) {
            return "Graph is empty";
        }

        String result = "";

        result += "Adjacency Matrix\n";
        result += "----------------\n";
        result += "index\t";

        for (int i = 0; i < numVertices; i++) {
            result += "" + i;
            if (i < 10) {
                result += " ";
            }
        }
        result += "\n\n";

        for (int i = 0; i < numVertices; i++) {
            result += "" + i + "\t";

            for (int j = 0; j < numVertices; j++) {
                if ((adjMatrix[i][j] > 0) && adjMatrix[i][j] < Double.POSITIVE_INFINITY) {
                    result += adjMatrix[i][j] + " ";
                } else {
                    result += "- ";
                }
            }
            result += "\n";
        }

        result += "\n\nVertex Values";
        result += "\n-------------\n";
        result += "index\tvalue\n\n";

        for (int i = 0; i < numVertices; i++) {
            result += "" + i + "\t";
            result += vertices[i].toString() + "\n";
        }
        result += "\n";
        return result;
    }

    /**
     * Adiciona uma aresta a este grafo, associando um valor entre 2 vertices. O
     * vertex1 em linhas, o vertex2 em colunas. Utiliza uma função getindex em
     * auxilio para obter o valor do index do vertice em questão. Overload da
     * função da função addEdge(int,int).
     *
     * @param vertex1 - primeiro vértice
     * @param vertex2 - segundo vértice
     */
    @Override
    public void addEdge(T vertex1, T vertex2) {
        addEdge(getIndex(vertex1), getIndex(vertex2));
    }

    /**
     * Insere uma aresta entre dois vértices do grafo. Utiliza a função
     * indexIsValid para verficar se o vertice é válido. Apena adiciona uma
     * aresta sendo o grafo direcional. Apenas adiciona uma aresta entre o
     * vertex1 e o vertex2.
     *
     * @param index1 indice do vertice inicial (primeiro)
     * @param index2 indice do vertice final (segundo)
     */
    public void addEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            this.adjMatrix[index1][index2] = 1;
        }
    }

    /**
     * Remove uma aresta entre 2 vertices do grafo. Não verifica se existe uma
     * aresta pois não é necessário Utiliza a função isIndexInvalid como auxilio
     * de verificação de erros. Atribui o valor Double.POSITIVE_INFINITY na
     * celula correspondente.
     *
     * @param index1 vertice inicial (origem)(linha)
     * @param index2 vertice final (destino)(coluna)
     */
    public void removeEdge(int index1, int index2) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            adjMatrix[index1][index2] = Double.POSITIVE_INFINITY;
        }
    }

    /**
     * Remove uma aresta entre 2 vertices. Aceita como parametro T(Comparable).
     * Utiliza o overload removeEdge(int,int),Com o auxilio da função getIndex
     * para obter o indice de cada vertice. Atribui o valor
     * Double.POSITIVE_INFINITY.
     *
     * @param vertex1 vertice inicial (origem)(linha)
     * @param vertex2 vertice final (destino)(coluna)
     */
    @Override
    public void removeEdge(T vertex1, T vertex2) {
        removeEdge(getIndex(vertex1), getIndex(vertex2));
    }

    /**
     * Adiciona um vertice ao array de vertices do grafo. Caso o numero de
     * vertices existentes seja maior ou igual que o tamanha do array de
     * vertices, invoca a função expandCapacity(). Como ainda não tem nenhuma
     * aresta adicionada, preenche na matriz a coluna e a linha correspondente
     * com Double.POSITIVE_INFINITY.
     *
     * @param vertex vertice a ser adicionado (Comparable).
     */
    @Override
    public void addVertex(T vertex) {
        if (numVertices >= vertices.length) {
            expandCapacity();
        }

        vertices[numVertices] = vertex;
        for (int i = 0; i <= numVertices; i++) {
            adjMatrix[numVertices][i] = Double.POSITIVE_INFINITY;
            adjMatrix[i][numVertices] = Double.POSITIVE_INFINITY;
        }

        numVertices++;
    }

    /**
     * Remove um vértice no índice fornecido do grafo. Caso o indice do vertice
     * nao seja válido não faz nada. Caso seja válido, remove o vertice do array
     * de vertices e remove os valores da matrix adjMatrix correspondetes ao
     * vertice removido.
     *
     * @param index indice do vertice a ser removido
     */
    public void removeVertex(int index) {
        if (indexIsValid(index)) {
            numVertices--;

            for (int i = index; i < numVertices; i++) {
                vertices[i] = vertices[i + 1];
            }

            for (int i = index; i < numVertices; i++) {
                for (int j = 0; j <= numVertices; j++) {
                    adjMatrix[i][j] = adjMatrix[i + 1][j];
                }
            }

            for (int i = index; i < numVertices; i++) {
                for (int j = 0; j < numVertices; j++) {
                    adjMatrix[j][i] = adjMatrix[j][i + 1];
                }
            }
        }
    }

    /**
     * Remove um vertice do grafo. Aceita como parâmetro um Comparable em vez de
     * um indice Invoca a função reoveVertex(int,int).
     *
     * @param vertex vertice a ser removido
     */
    @Override
    public void removeVertex(T vertex) {
        for (int i = 0; i < numVertices; i++) {
            if (vertex.equals(vertices[i])) {
                removeVertex(i);
                return;
            }
        }
    }

    /**
     * Retorna um primeiro iterador de profundidade começando com o vértice
     * dado.
     *
     * @param startVertex - vértice inicial
     * @return um primeiro iterador de profundidade começando no vértice dado
     * @throws EmptyCollectionException if an empty collection exception occurs
     */
    @Override
    public Iterator<T> iteratorDFS(T startVertex) throws EmptyCollectionException {
        return iteratorDFS(getIndex(startVertex));
    }

    /**
     * Obter um iterador DFS com a pesquisa em profundidade a partir de um
     * índice
     *
     * @param startIndex índice onde a pesquisa começará
     * @return um iterador.
     * @throws EmptyCollectionException é lançada se a stack estiver vazia
     */
    public Iterator<T> iteratorDFS(int startIndex) throws EmptyCollectionException {
        Integer x;
        boolean found;
        ArrayStack<Integer> traversalStack = new ArrayStack<>();
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
        boolean[] visited = new boolean[numVertices];

        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }

        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalStack.push(startIndex);
        resultList.addTOrear(vertices[startIndex]);
        visited[startIndex] = true;

        while (!traversalStack.isEmpty()) {
            x = traversalStack.peek();
            found = false;

            /**
             * Encontre um vértice adjacente a x que não tenha sido visitado e
             * empurre-o na pilha
             */
            for (int i = 0; (i < numVertices) && !found; i++) {
                if (adjMatrix[x][i] > 0 && !visited[i]) {
                    traversalStack.push(i);
                    resultList.addTOrear(vertices[i]);
                    visited[i] = true;
                    found = true;
                }
            }
            if (!found && !traversalStack.isEmpty()) {
                traversalStack.pop();
            }
        }
        return resultList.iterator();
    }

    /**
     * Retorna um primeiro iterador de profundidade começando com o vértice
     * dado.
     *
     * @param startVertex - vértice inicial
     * @return um primeiro iterador de profundidade começando no vértice dado
     * @throws EmptyCollectionException if an empty collection exception occurs
     */
    @Override
    public Iterator<T> iteratorBFS(T startVertex) throws EmptyCollectionException {
        return iteratorBFS(getIndex(startVertex));
    }

    /**
     * BFS Iterator
     *
     * @param startIndex - índice de inicio
     * @return iterador com lista de itens visitados
     * @throws EmptyCollectionException é lançada quando a queue está vazia
     */
    public Iterator<T> iteratorBFS(int startIndex) throws EmptyCollectionException {
        Integer x;
        LinkedQueue<Integer> traversalQueue = new LinkedQueue<>();
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();

        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;

        while (!traversalQueue.isEmpty()) {
            x = traversalQueue.dequeue();
            resultList.addTOrear(vertices[x]);

            /* Encontra todos os vértices adjacentes a x que não tenham sido 
            visitados*/
            for (int i = 0; i < numVertices; i++) {
                if (adjMatrix[x][i] > 0 && !visited[i]) {
                    traversalQueue.enqueue(i);
                    visited[i] = true;
                }
            }
        }
        return resultList.iterator();
    }

    /**
     *
     * Retorna um iterador que contém o caminho mais curto entre os dois
     * vértices.
     *
     * @param startVertex - vértice inicial
     * @param targetVertex - vértice final
     * @return um iterador que contém o caminho mais curto entre os dois
     * vértices
     * @throws EmptyCollectionException if an empty collection exception occurs
     */
    @Override
    public Iterator<T> iteratorShortestPath(T startVertex, T targetVertex) throws EmptyCollectionException {

        return iteratorShortestPath(getIndex(startVertex), getIndex(targetVertex));
    }

    /**
     * Obter um iterador com a lista de locais entre dois vertices
     *
     * @param startIndex - índice de início
     * @param targetIndex - índice de paragem
     * @return um iterador
     * @throws Exeception.EmptyCollectionException
     */
    public Iterator<T> iteratorShortestPath(int startIndex, int targetIndex) throws EmptyCollectionException {
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)) {
            return resultList.iterator();
        }

        Iterator<Integer> it = iteratorShortestPathIndices(startIndex,
                targetIndex);
        while (it.hasNext()) {
            resultList.addTOrear(vertices[(it.next())]);
        }
        return resultList.iterator();
    }

    /**
     * Retorna um iterador que contém os índices dos vértices que estão no
     * caminho mais curto entre os dois vértices dados.
     *
     * @param startIndex indice do vertice inicial de pesquiza
     * @param targetIndex indice do vertices final de pesquiza
     * @return Um iterador de inteiros com os indices dos vertices
     *
     * @throws EmptyCollectionException
     */
    protected Iterator<Integer> iteratorShortestPathIndices(int startIndex, int targetIndex) throws EmptyCollectionException {
        int index = startIndex;
        int[] pathLength = new int[numVertices];
        int[] predecessor = new int[numVertices];
        LinkedQueue<Integer> traversalQueue = new LinkedQueue<>();
        ArrayUnorderedList<Integer> resultList
                = new ArrayUnorderedList<>();

        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)
                || (startIndex == targetIndex)) {
            return resultList.iterator();
        }

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;
        pathLength[startIndex] = 0;
        predecessor[startIndex] = -1;

        while (!traversalQueue.isEmpty() && (index != targetIndex)) {
            index = (traversalQueue.dequeue());

            /**
             * Atualiza o pathLength para cada vértice não visitado adjacente ao
             * vértice no índice atual.
             */
            for (int i = 0; i < numVertices; i++) {
                if (adjMatrix[index][i] > 0 && !visited[i]) {
                    pathLength[i] = pathLength[index] + 1;
                    predecessor[i] = index;

                    traversalQueue.enqueue(i);
                    visited[i] = true;
                }
            }
        }
        if (index != targetIndex) {
            return resultList.iterator();
        }

        StackList<Integer> stack = new StackList<>();
        index = targetIndex;
        stack.push(index);
        do {
            index = predecessor[index];
            stack.push(index);
        } while (index != startIndex);

        while (!stack.isEmpty()) {
            resultList.addTOrear((stack.pop()));
        }

        return resultList.iterator();
    }

    /**
     * Retorna o peso do caminho de menor peso na rede. Retorna 0 se nenhum
     * caminho for encontrado.
     *
     * @param startIndex indice do vertice inicial
     * @param targetIndex indice do vertice final
     * @return o numero de nos visitado do caminho percorrido
     * @throws Exeception.EmptyCollectionException
     */
    public int shortestPathLength(int startIndex, int targetIndex) throws EmptyCollectionException {
        int result = 0;
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)) {
            return 0;
        }

        int index1, index2;
        Iterator<Integer> it = iteratorShortestPathIndices(startIndex,
                targetIndex);

        if (it.hasNext()) {
            index1 = (it.next());
        } else {
            return 0;
        }

        while (it.hasNext()) {
            result++;
            it.next();
        }

        return result;
    }

    /**
     * Retorna o peso do caminho de menor peso na rede. Retorna 0 se nenhum
     * caminho for encontrado.
     *
     * @param startVertex
     * @param targetVertex
     * @return
     * @throws Exeception.EmptyCollectionException
     */
    public int shortestPathLength(T startVertex, T targetVertex) throws EmptyCollectionException {

        return shortestPathLength(getIndex(startVertex), getIndex(
                targetVertex));
    }

    /**
     * Retorna uma árvore de amplitude mínima do grafo.(Minimun Spawn Tree)
     *
     * @return um grafo pesado baseado numa arvore
     * @throws Exeception.EmptyCollectionException
     */
    public Network getMST() throws EmptyCollectionException {
        int x, y;
        int[] edge = new int[2];
        StackList<int[]> vertexStack = new StackList<>();
        Network<T> resultGraph = new Network<>();

        if (isEmpty() || !isConnected()) {
            return resultGraph;
        }

        resultGraph.adjMatrix = new double[numVertices][numVertices];

        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                resultGraph.adjMatrix[i][j] = 0;
            }
        }

        resultGraph.vertices = (T[]) (new Comparable[numVertices]);
        boolean[] visited = new boolean[numVertices];

        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        edge[0] = 0;
        resultGraph.vertices[0] = this.vertices[0];
        resultGraph.numVertices++;
        visited[0] = true;

        /**
         * Adicione todas as arestas adjacentes ao vértice 0 à pilha.
         */
        for (int i = 0; i < numVertices; i++) {
            if (!visited[i] && this.adjMatrix[0][i] > 0) {
                edge[1] = i;
                vertexStack.push(edge.clone());
                visited[i] = true;
            }
        }

        while ((resultGraph.size() < this.size()) && !vertexStack.isEmpty()) {
            edge = vertexStack.pop();
            x = edge[0];
            y = edge[1];
            resultGraph.vertices[y] = this.vertices[y];
            resultGraph.numVertices++;
            resultGraph.adjMatrix[x][y] = 1;
            resultGraph.adjMatrix[y][x] = 1;
            visited[y] = true;

            /**
             * Adicione todas as arestas não visitadas adjacentes ao vértice da
             * pilha
             */
            for (int i = 0; i < numVertices; i++) {
                if (!visited[i] && this.adjMatrix[i][y] > 0) {
                    edge[0] = y;
                    edge[1] = i;
                    vertexStack.push(edge.clone());
                    visited[i] = true;
                }
            }
        }

        return resultGraph;
    }

    /**
     * Cria novos arrays para armazenar o conteúdo do grafo com o dobro da
     * capacidade. Expande o array de vertices para o dobro assim como a matrix
     * de adjacências em linhas e colunas.
     *
     */
    private void expandCapacity() {
        T[] largerVertices = (T[]) (new Comparable[vertices.length * 2]);
        double[][] largerAdjMatrix
                = new double[vertices.length * 2][vertices.length * 2];

        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                largerAdjMatrix[i][j] = adjMatrix[i][j];
            }
            largerVertices[i] = vertices[i];
        }

        vertices = largerVertices;
        adjMatrix = largerAdjMatrix;
    }

    /**
     * Retorna o número de vértices no grafo.
     *
     * @return numVertices numero de vertices no array
     */
    @Override
    public int size() {
        return numVertices;
    }

    /**
     * Retorna true se o grafo estiver vazio e false caso contrário.
     *
     * @return true se o array de vertices tiver vazio. False caso tenha algum
     * vertice no array.
     */
    @Override
    public boolean isEmpty() {
        return numVertices == 0;
    }

    /**
     * Verifica se o grafo é conexo.
     *
     * @return True se for conexo. False se nao for conexo
     * @throws EmptyCollectionException
     */
    @Override
    public boolean isConnected() throws EmptyCollectionException {
        if (isEmpty()) {
            return false;
        }

        Iterator<T> it = iteratorBFS(0);
        int count = 0;

        while (it.hasNext()) {
            it.next();
            count++;
        }
        return (count == numVertices);
    }

    /**
     * Verifica se um grafo é conexo a partir de um vertice à escolha
     *
     * @param Startvertex vertice inicial de pesquiza
     * @return True se o grafo for conexo. False se não for conexo
     * @throws EmptyCollectionException
     */
    public boolean isConnectedCustom(T Startvertex) throws EmptyCollectionException {
        if (isEmpty()) {
            return false;
        }

        Iterator<T> it = iteratorBFS(getIndex(Startvertex));
        int count = 0;

        while (it.hasNext()) {
            it.next();
            count++;
        }
        return (count == numVertices);
    }

    /**
     * Retorna o valor do índice da primeira ocorrência do vértice.
     *
     * @param vertex vertice a ser pesquizado no array
     * @return indice do vertice. -1 caso não seja encontrado
     */
    public int getIndex(T vertex) {
        for (int i = 0; i < numVertices; i++) {
            if (vertices[i].equals(vertex)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Verifica se o index do vertice está dentro do valores limite do array de
     * vertices
     *
     * @param index inteiro a ser verificado
     * @return returna true se estiver dentro dos valores de array validos.
     * entre 0 e numVertices.
     */
    protected boolean indexIsValid(int index) {
        return ((index < numVertices) && (index >= 0));
    }

    /**
     * Getter do array vértices do grafo.
     *
     * @return array de vertices
     */
    public T[] getVertices() {
        return vertices;
    }

    /**
     * Pesquiza no array de vertices se algum contem o email desejado.
     *
     * @param em Email a ser pesquizado
     * @return retorna o primeiro utilizador com o e-mail correspondente. Caso
     * não exista retorna um utilizador vazio
     */
    public Utilizador getByEmail(String em) {
        int index = 0;

        while (index < numVertices) {
            Utilizador ut = (Utilizador) this.vertices[index];
            if (ut.getEmail().equals(em)) {
                return ut;
            }
            index++;
        }
        return new Utilizador();
    }

    /**
     * Verifica se o grafo é completo. se todo o vertice está conectado a todos
     * os outros vertices. Cada vertices deverá ter numero de arestas =
     * numVertices -1;
     *
     * @return retorna True se o grafo estiver completo. False se não estiver
     * completo.
     */
    public boolean isComplete() {

        int count = 0;
        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                if (adjMatrix[i][j] > 0 && adjMatrix[i][j] < Double.POSITIVE_INFINITY) {
                    count++;
                }
            }
        }

        return count == ((numVertices * numVertices) - numVertices);
    }

    /**
     * Funcionalidade que aceita um parametro T da função UtilizadorNotConected.
     *
     * @param startvertex vertice a ser verificado
     * @return um iterador de uma lista dos vertices não alcansáveis
     */
    public Iterator<T> UtilizadorNotConected(T startvertex) {
        return UtilizadorNotConected(getIndex(startvertex));
    }

    /**
     * Verifica os utilizadores que não são alcansáveis a partir de um
     * utilizador enviado por argumento
     *
     * @param startVertex vertice inicial de pesquiza
     * @return um iterador
     */
    public Iterator<T> UtilizadorNotConected(int startVertex) {
        UnorderedLinkedList result = new UnorderedLinkedList<>();
        UnorderedLinkedList compare = new UnorderedLinkedList<>();

        Iterator itr = null;

        try {
            itr = iteratorBFS(startVertex);
        } catch (EmptyCollectionException ex) {
            ex.printStackTrace();
        }

        while (itr.hasNext()) {
            compare.addTOrear((Utilizador) itr.next());
        }

        for (int i = 0; i < compare.size(); i++) {
            if (!compare.contains(this.vertices[i])) {
                result.addTOrear(vertices[i]);
            }
        }

        return result.iterator();
    }

    /**
     * Retorna um iterador de uma lista que contem os utilizadores que tem como
     * skill ou trabalha na empresa.
     *
     * @param startVertex vertice inicial de pesquiza
     * @param search String de pesquiza nas skills ou empresa
     * @return returna um iterador de uma lista de utilizadores que contem a
     * palavra chave
     */
    public Iterator<T> sameSKillsorCompany(T startVertex, String search) {
        return sameSkillsorCompany(getIndex(startVertex), search);
    }

    /**
     * Retorna um iterador de uma lista que contem os utilizadores que tem como
     * skill ou trabalha na empresa. Aceita um inteiro index
     *
     * @param startIndex vertice inicial
     * @param search string a procurar
     * @return um iterador de uma lista com os resultados
     */
    public Iterator<T> sameSkillsorCompany(int startIndex, String search) {

        ArrayStack<Utilizador> temp = new ArrayStack<>();
        ArrayStack<Integer> savePos = new ArrayStack<>(this.vertices.length);
        UnorderedLinkedList<Utilizador> returnsUt = new UnorderedLinkedList<>();

        for (int i = 0; i < this.adjMatrix[startIndex].length; i++) {
            if (this.adjMatrix[startIndex][i] > 0 && this.adjMatrix[startIndex][i] < Double.POSITIVE_INFINITY) {
                savePos.push(i);
            }
        }

        while (!savePos.isEmpty()) {
            for (int j = 0; j < this.vertices.length; j++) {
                if (this.adjMatrix[j][savePos.peek()] > 0 && this.adjMatrix[j][savePos.peek()] < Double.POSITIVE_INFINITY ) {
                    temp.push((Utilizador) this.vertices[j]);
                }
            }
            savePos.pop();
        }

        while (!temp.isEmpty()) {
            Iterator<Profissional> itr = temp.peek().getProfissional().iterator();
            if (temp.peek().getSkills().contains(search) || itr.next().getEmpresa().equals(search)) {
                returnsUt.addTOrear(temp.peek());
            }
            temp.pop();
        }

        return (Iterator<T>) returnsUt.iterator();
    }

    /**
     * Verifica os utilizadores que não se encontram na mesma empresa
     *
     * @param ut vertice inicial de pesquiza
     * @param company empresa a ser pesquizada
     * @return um iterador com os utilizadores que vao ao encontro dos
     * requisitos
     */
    public Iterator NotsameCompany(T ut, String company) {
        UnorderedLinkedList<Utilizador> result = new UnorderedLinkedList();

        Iterator<Utilizador> itr = this.sameCompany(ut, company);

        while (itr.hasNext()) {
            Iterator<Profissional> itr2 = itr.next().getProfissional().iterator();
            if (!itr2.next().getEmpresa().equals(company)) {
                result.addTOrear(itr.next());
            }
        }

        return result.iterator();
    }

    /**
     * Retorna um Utilizador com o id pretendido
     *
     * @param id id a pesquizar nos utilizadores
     * @return um utilizador com o id caso seja encontrado. Caso não exista
     * retorna um utilizador vazio
     */
    public Utilizador getById(int id) {

        int counter = 0;

        //copia do array de vertices para um array de utilizadores devido ao cast
//        for (int i = 0; i < this.vertices.length; i++) {
//            ut[i] = (Utilizador) this.vertices[i];
//        }
        while (counter < numVertices) {
            Utilizador ut = (Utilizador) this.vertices[counter];
            if (ut.getIdUtilizador() == id) {
                return ut;
            }
            counter++;
        }

        return new Utilizador();
    }

    /**
     * Verifica os vertices alcansáveis a partir de um vertice inicial e que
     * pertençam a mesma empresa
     *
     * @param ut
     * @param company
     * @return
     */
    public Iterator sameCompany(T ut, String company) {
        Iterator<Utilizador> itr = null;
        Iterator<Profissional> itr2 = null;

        try {
            itr = (Iterator<Utilizador>) this.iteratorBFS(ut);
        } catch (EmptyCollectionException ex) {
            ex.printStackTrace();
        }

        UnorderedLinkedList<Utilizador> result = new UnorderedLinkedList();

        while (itr.hasNext()) {
            Utilizador nu = itr.next();
            itr2 = nu.getProfissional().iterator();

            while (itr2.hasNext()) {
                Profissional p = itr2.next();
                if (company.equals(p.getEmpresa())) {
                    result.addTOrear(nu);
                }
            }
        }

        return result.iterator();
    }

    public Utilizador getUtilizadorFromindex(int index) {
        return (Utilizador) this.vertices[index];
    }
}
