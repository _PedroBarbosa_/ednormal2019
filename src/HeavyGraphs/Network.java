/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HeavyGraphs;

import Exeception.EmptyCollectionException;
import Interfaces.NetworkADT;
import Json.JsonRead;
import ed_trabalhopraticoen.Utilizador;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Runo
 * @param <T>
 */
public class Network<T extends Comparable<? super T>> extends Graph<T> implements NetworkADT<T> {

    public Network() {
        super();

    }

    @Override
    public void addEgde(T vertex1, T vertex2, double popularidade) {
        addEdge(super.getIndex(vertex1), super.getIndex(vertex2), popularidade);
    }

    public void addEdge(int index1, int index2, double weight) {
        if (indexIsValid(index1) && indexIsValid(index2)) {
            //super.adjMatrix[index1][index2] = weight;
            super.adjMatrix[index2][index1] = weight;

        }
    }

    @Override
    public double shortestPathWeight(T startVertex, T targetVertex) {
        return shortestPathWeight(getIndex(startVertex), getIndex(targetVertex));
    }

    /**
     * Caminho mais curto por peso
     *
     * @param StartVertex
     * @param EndVertex
     * @return
     */
    public double shortestPathWeight(int StartVertex, int EndVertex) {
        double result = 0;
        if (!indexIsValid(StartVertex) || !indexIsValid(EndVertex)) {
            return Double.POSITIVE_INFINITY;
        }

        int index1, index2;
        try {
            Iterator<Integer> it = iteratorShortestPathIndices(StartVertex,
                    EndVertex);

            if (it.hasNext()) {
                index1 = (it.next());
            } else {
                return Double.POSITIVE_INFINITY;
            }

            while (it.hasNext()) {
                index2 = (it.next());
                result += adjMatrix[index1][index2];
                index1 = index2;
            }
        } catch (EmptyCollectionException e) {
            e.printStackTrace();
        }
        return result;
    }

    public double[] shortestPathWeightArray(int StartVertex, int EndVertex) {
        double result[] = new double[vertices.length];
        if (!indexIsValid(StartVertex) || !indexIsValid(EndVertex)) {
            return null;
        }

        int index1, index2, i = 0;
        double temp = 0;
        try {
            Iterator<Integer> it = iteratorShortestPathIndices(StartVertex,
                    EndVertex);

            if (it.hasNext()) {
                index1 = (it.next());
            } else {
                return null;
            }

            while (it.hasNext()) {
                index2 = (it.next());
                temp += adjMatrix[index1][index2];
                result[i] = temp;
                index1 = index2;
                i++;
            }
        } catch (EmptyCollectionException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void loadJSONfile() {
        JsonRead read = new JsonRead();
        try {
            read.LerJson(this);
            //read.listarUtilizadores();
        } catch (IOException ex) {
            Logger.getLogger(Graph.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(Graph.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public String toString() {
        return super.toString();
    }

    public double[] UtilizadoresBySkillWeight(T ut, String skill) throws EmptyCollectionException {

        double[] weigths = new double[vertices.length];
        Iterator<T> itr = iteratorDFS(ut);
        weigths = shortestPathWeightArray(getIndex(ut), getIndex(itr.next()));

        return weigths;
    }

    @Override
    public T[] getVertices() {
        return super.getVertices();
    }
    
    public Utilizador getByemail(String email){
        return super.getByEmail(email);
    }
}
