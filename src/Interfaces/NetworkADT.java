/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import ed_trabalhopraticoen.Utilizador;

/**
 *
 * @author rafaelsantos
 * @param <T>
 */

public interface NetworkADT<T>  extends GraphADT<T>
{
    public void addEgde(T vertex1, T vertex2, double popularidade);

    public double shortestPathWeight(T StartVertex, T EndVertex);
    
    public void loadJSONfile(); 
    
    public T[] getVertices();
    
    @Override
    public String toString();
    
    public Utilizador getByemail(String email);

}