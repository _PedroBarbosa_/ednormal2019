/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import ed_trabalhopraticoen.Utilizador;

/**
 *
 * @author rafaelsantos
 * @param <T>
 */
public interface UtilizadorInterface extends Comparable<Utilizador> {
    
    public double increaseVis();
    
    @Override
    public String toString();
    
    @Override
    public int compareTo(Utilizador id);
}
