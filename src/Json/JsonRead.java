/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Json;

import Arrays.OrderedArray;
import HeavyGraphs.Network;
import InterfaceGrafica.DadosUtilizador;
import Lists.UnorderedLinkedList;
import ed_trabalhopraticoen.FormacaoAcademica;
import ed_trabalhopraticoen.Profissional;
import ed_trabalhopraticoen.Utilizador;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author rafaelsantos
 */
public class JsonRead {

    private UnorderedLinkedList<Utilizador> user;
    //private UnorderedLinkedList<Long> listContacts;

    public void LerJson(Network rede) throws IOException, ParseException {
        
        user = new UnorderedLinkedList<>();
        //listContacts = new UnorderedLinkedList<>();
        try {
            JSONParser parser = new JSONParser();

            Object obj = parser.parse(new FileReader("socialgraph.json"));

            JSONObject jsonObject = (JSONObject) obj;

            //loop array para o Grafo Social
            JSONArray grafoSocial = (JSONArray) jsonObject.get("grafoSocial");
            Iterator iterator_grafoSocial = grafoSocial.iterator();

            while (iterator_grafoSocial.hasNext()) {
                JSONObject utilizador = (JSONObject) iterator_grafoSocial.next();

                long id = (long) utilizador.get("id");
                String nome = (String) utilizador.get("nome");
                long idade = (long) utilizador.get("idade");
                String email = (String) utilizador.get("email");

                //System.out.println("Grafo completo "+utilizador);
                //loop array para a formação academica
                OrderedArray<FormacaoAcademica> arrayFormacao = new OrderedArray<>();
                JSONArray formacaoAcademica = (JSONArray) utilizador.get("formacaoAcademica");
                Iterator iterator_formacaoAcademica = formacaoAcademica.iterator();

                while (iterator_formacaoAcademica.hasNext()) {
                    JSONObject formacao_academica = (JSONObject) iterator_formacaoAcademica.next();

                    Long ano = (long) formacao_academica.get("ano");
                    String formacao = (String) formacao_academica.get("formacao");

                    FormacaoAcademica formacaoTemp = new FormacaoAcademica(ano.intValue(), formacao);

                    try {

                        arrayFormacao.add(formacaoTemp);

                    } catch (Exception ex) {
                        System.out.println("Erro ao ler os dados da formação academica");
                    }

                    //System.out.println("Formação academica"+formacao_academica);
                }
                //loop array para os cargos profissioais
                OrderedArray<Profissional> arrayProfissional = new OrderedArray<>();
                JSONArray cargosProfissionais = (JSONArray) utilizador.get("cargosProfissionais");
                Iterator iterator_cargosProfissionais = cargosProfissionais.iterator();

                while (iterator_cargosProfissionais.hasNext()) {
                    JSONObject cargos_profissionais = (JSONObject) iterator_cargosProfissionais.next();

                    Long ano_cargo = (long) cargos_profissionais.get("ano");
                    String cargo = (String) cargos_profissionais.get("cargo");
                    String empresa = (String) cargos_profissionais.get("empresa");

                    Profissional profissionalTemp = new Profissional(ano_cargo.intValue(), cargo, empresa);

                    try {
                        arrayProfissional.add(profissionalTemp);

                    } catch (Exception exception) {
                        System.out.println("Erro ao ler os cargos profissionais");
                    }

                    //System.out.println("Cargos Profissionais "+cargos_profissionais);
                }

                //loop array para as skills
                UnorderedLinkedList<String> listSkills = new UnorderedLinkedList<>();
                JSONArray skills = (JSONArray) utilizador.get("skills");
                Iterator iterator_skills = skills.iterator();

                while (iterator_skills.hasNext()) {

                    String _skills = (String) iterator_skills.next();

                    try {
                        listSkills.addTOrear((_skills));

                    } catch (Exception exception) {
                        System.out.println("Erro ao ler as skills");
                    }

                    //System.out.println("Skils " + _skills);
                }

                //loop array para a contacts
                 UnorderedLinkedList<Integer> listContacts = new UnorderedLinkedList<>();
                JSONArray contacts = (JSONArray) utilizador.get("contacts");
                Iterator iterator_contacts = contacts.iterator();
               
                
                while (iterator_contacts.hasNext()) {

                    JSONObject listaContactos = (JSONObject) iterator_contacts.next();

                    long userid = (long) listaContactos.get("userid");

                    try {
                        listContacts.addTOfront((int)userid);

                    } catch (Exception exception) {
                        System.out.println("Erro ao ler os dados da lista de contactos");
                    }

                    //System.out.println("Contactos    "+listaContactos);
                }

                //loop array para a mencoes
                UnorderedLinkedList<Long> listMencoes = new UnorderedLinkedList<>();
                JSONArray mencoes = (JSONArray) utilizador.get("mencoes");
                Iterator iterator_mencoes = mencoes.iterator();

                while (iterator_mencoes.hasNext()) {

                    JSONObject listaMensoes = (JSONObject) iterator_mencoes.next();

                    Long user = (long) listaMensoes.get("userid");

                    try {
                        listMencoes.addTOfront(user);

                    } catch (Exception exception) {
                        System.out.println("Erro ao ler os dados da lista de contactos");
                    }

                    //System.out.println("Menções     "+listaMensoes);
                }

                //Visualizações
                Long visualizacoes = (Long) utilizador.get("visualizacoes");

                //System.out.println("Visualizações    " + visualizacoes);
                user.addTOfront(new Utilizador((int) id, nome, (int) idade, email, arrayFormacao, arrayProfissional, listSkills, listMencoes, listContacts, Math.toIntExact(visualizacoes)));
                rede.addVertex(user.last());
               
               
            }
            criarArestas(rede);

        } catch (FileNotFoundException ex) {
            Logger.getLogger(JsonRead.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void criarArestas(Network rede) {

        
         
        Iterator  iterador_user = user.iterator();
        while (iterador_user.hasNext()) {

            Utilizador user_next = (Utilizador) iterador_user.next();
            Iterator it = user_next.getContactos().iterator();
            
            while (it.hasNext()) {
                
                int next = (int) it.next();
  
                rede.addEgde(user_next, rede.getById(next), 1 / user_next.getNumeroVisualizacoes());

            }

        }
    }
    
    public void listarUtilizadores()
    {
        Iterator listar_utilizadores = user.iterator();
        while(listar_utilizadores.hasNext())
        {
            Utilizador user_lista = (Utilizador) listar_utilizadores.next();
            System.out.println(user_lista.toString());
            Iterator listar_formacao = user_lista.getFormacaoAcademica().iterator();
            while(listar_formacao.hasNext())
            {
                System.out.println(listar_formacao.next());
            }
            Iterator lista_cargos = user_lista.getProfissional().iterator();
            while(lista_cargos.hasNext())
            {
               System.out.println(lista_cargos.next());
            }
            Iterator listar_skills = user_lista.getSkills().iterator();
            while(listar_skills.hasNext())
            {
                System.out.println(listar_skills.next());
            }
            Iterator listar_contactos = user_lista.getContactos().iterator();
            while(listar_contactos.hasNext())
            {
                System.out.println("Contacto:"+listar_contactos.next());
            }
            Iterator lista_mencoes = user_lista.getMencoes().iterator();
            while(lista_mencoes.hasNext())
            {
                System.out.println("Menções:"+lista_mencoes.next());
            }
            
        }
        
        
    }
    
//    public void listarUtilizadoresPorEmail(Network rede) 
//    {
//       
//        
//        
//        
//
//    }
    
    

}
