/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed_trabalhopraticoen;

import Arrays.OrderedArray;
import HeavyGraphs.Network;
import Interfaces.NetworkADT;
import Lists.UnorderedLinkedList;
import java.util.Iterator;

/**
 *
 * @author rafaelsantos
 */
public class ED_TrabalhoPraticoEN {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Network<Utilizador> n = new Network<>();
        OrderedArray<Profissional> p1 = new OrderedArray<>();
        Profissional p = new Profissional(0, "sdfda", "empresa");
        
        OrderedArray f1 = new OrderedArray();
        FormacaoAcademica f = new FormacaoAcademica(2000, "qualquer");
        FormacaoAcademica a = new FormacaoAcademica(2007, "LEI");
        f1.add(f);
        f1.add(a);
        UnorderedLinkedList s1 = new UnorderedLinkedList();
        s1.addTOrear("java");
        UnorderedLinkedList men = new UnorderedLinkedList();
        men.addTOrear("user 1");
        UnorderedLinkedList tel1 = new UnorderedLinkedList();
        tel1.addTOrear("984654846");
        OrderedArray<Profissional> p2 = new OrderedArray<>();
        Profissional pnovo = new Profissional(1234, "nsei", "mmmmmmm");
        p2.add(pnovo);
        p1.add(pnovo);
        p2.add(p);
        
        Utilizador ut1 = new Utilizador(1, "test", 20, "asfsdfas", f1, p1, s1, men, tel1, 10);
        n.addVertex(ut1);
        Utilizador ut2 = new Utilizador(2, "ut2", 21, "asdafsd", f1, p1, tel1, men, tel1, 5);
        n.addVertex(ut2);
        Utilizador ut3 = new Utilizador(3, "ut3", 27, "vbcncgnh", f1, p2, tel1, men, tel1, 15);
        n.addVertex(ut3);
//        n.addEgde(ut3, ut1, 9000);
//        n.addEgde(ut1, ut3, 20);
//        
//        n.addEgde(ut1, ut2, 30);
//        n.addEgde(ut2, ut3, 100);
      //  n.addEgde(ut3, ut2, 10);
      //  n.addEgde(ut2, ut1, 11);
        //Iterator itr = n.sameCompany(ut1,"empresa");
//        System.out.println(n.toString());
//
//        NetworkADT rede = new Network();
//        rede.loadJSONfile();
//        System.out.println(rede.toString());

        Iterator itr = n.sameSKillsorCompany(ut1, "empresa");
        
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }
    
}
