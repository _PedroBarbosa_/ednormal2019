/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed_trabalhopraticoen;

/**
 *
 * @author rafaelsantos
 */
public class FormacaoAcademica implements Comparable<FormacaoAcademica>
{
    private int ano;
    private String nome;

    public FormacaoAcademica(int ano, String nome) 
    {
        this.ano = ano;
        this.nome = nome;
    }

    /**
     * Getter genérico da variavel ano
     * @return ano da formação academica do utilizador
     */
    public int getAno() {
        return ano;
    }

    /**
     * Setter genérico da variavel ano
     * @param ano ano a ser definido do curso
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    /**
     * getter genérico da variavel nome
     * @return 
     */
    public String getNome() {
        return nome;
    }

    /**
     * Setter genérico da variavel nome
     * @param nome nome da formação academica
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * overrides toString. imprime ano + nome
     * @return retorna uma String com o ano e o nome da formação
     */
    @Override
    public String toString() {
        return "Ano:" + this.ano + "\nCargo:" + this.nome ;
    }

    /**
     * metodo que compara entre 2 formação para que estes sejam comparaveis e possiveis de organizacao facilitada
     * @param o formacaoAcademica a comparar 
     * @return retorna 1 se o for menor que esta instância
     * retorna 0 se for igual
     * retorna -1 se for maior que esta instância
     */
    @Override
    public int compareTo(FormacaoAcademica o) {
        if(this.ano > o.getAno())
            return 1;
        else if(this.ano == o.getAno())
            return 0;
        else
            return -1;
    }
    
}
