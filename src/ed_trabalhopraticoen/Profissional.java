/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed_trabalhopraticoen;

/**
 *
 * @author rafaelsantos
 */
public class Profissional implements Comparable<Profissional>
{
    private int ano;
    private String cargo;
    private String empresa;

    public Profissional(int ano, String cargo, String empresa) 
    {
        this.ano = ano;
        this.cargo = cargo;
        this.empresa = empresa;
    }

    /**
     * Getter genérico da variavel ano
     * @return ano da formação academica do utilizador
     */
    public int getAno() {
        return ano;
    }

    /**
     * Setter genérico da variavel ano
     * @param ano ano a ser definido do curso
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    /**
     * Getter genérico da variavel cargo
     * @return returna uma string com o cargo desempenhado
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * Setter genérico da variavel cargo
     * @param cargo String a definir
     */
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    /**
     * Getter genérico da variavel empresa
     * @return 
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Setter genérico da variavel empresa
     * @param empresa String a definir 
     */
    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }
    
    /**
     * overrides toString.
     * @return devolve uma string com o ano + cargo + empresa da class Profissional
     */
    @Override
    public String toString() {
        return "Ano:" + ano +"\nCargo:" + cargo + " \nEmpresa:" + empresa;
    }

    /**
     * overrides compareTo.
     * @param o profissional da ser comparado com esta instancia
     * @return retorna um intereiro conforme o ano do cargo desempenhado
     * retorna 1 caso o ano desta instancia seja maior que o argumento
     * retorna 0 caso o ano desta instancia seja igual ao argumento
     * retorna -1 caso o ano desta instancia seja menor que o argumento
     */
    @Override
    public int compareTo(Profissional o) {
         if(this.ano > o.getAno())
            return 1;
        else if(this.ano == o.getAno())
            return 0;
        else
            return -1;
    }
    
}
