/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed_trabalhopraticoen;

import Interfaces.UtilizadorInterface;
import Arrays.OrderedArray;
import Lists.UnorderedLinkedList;
/**
 *
 * @author rafaelsantos
 * @param <T>
 */
public class Utilizador implements UtilizadorInterface, Comparable<Utilizador> {

    private int idUtilizador;
    private String nome;
    private int idade;
    private char sexo;
    private String email;
    // Array com uma lista ordenada das formações academica dividida por ano e nome
    private OrderedArray<FormacaoAcademica> formacao;
    //Array com uma lista ordenadadas profissões dividido por ano, cargo e empresa 
    private OrderedArray<Profissional> empregos;
    //Lista com as skills
    private UnorderedLinkedList<String> skills;
    //Array com uma lista de mencoes
    private UnorderedLinkedList<Integer> mencoes;
    //Lista de contactos 
    private UnorderedLinkedList<Integer> contactos;
    //Numero de visualizações
    private double numeroVisualizacoes;

    public Utilizador(int idUtilizador, String Nome, int Idade,String Email, OrderedArray FormacaoAcademica, OrderedArray Profissional, UnorderedLinkedList Skills, UnorderedLinkedList mencoes, UnorderedLinkedList contactos, double numeroVisualizacoes) {
        this.idUtilizador = idUtilizador;
        this.nome = Nome;
        this.idade = Idade;
        this.email = Email;
        this.formacao = FormacaoAcademica;
        this.empregos = Profissional;
        this.skills = Skills;
        this.mencoes = mencoes;
        this.contactos = contactos;
        this.numeroVisualizacoes = numeroVisualizacoes;
    }

    public Utilizador() {
    }
    
    

    /**
     *
     * @return aumenta o numero de visualizações e retorna-o
     */ 
    @Override
    public double increaseVis() {
        return this.numeroVisualizacoes++;
    }

    /**
     * Comparador genérico da classe utilizador
     *
     * @param ut utilizador a comparar
     * @return retorna 1 caso ut seja menor, 0 caso seja igual e -1 caso seja
     * superior
     */
    @Override
    public int compareTo(Utilizador ut) {
        if (this.idUtilizador > ut.getIdUtilizador()) {
            return 1;
        } else if (this.idUtilizador == ut.getIdUtilizador()) {
            return 0;
        } else {
            return -1;
        }
    }

    /**
     * getter genérico do idUtilizador
     *
     * @return
     */
    public int getIdUtilizador() {
        return idUtilizador;
    }

    /**
     * Setter genérico da variavel idUtilizador
     *
     * @param idUtilizador
     */
    public void setIdUtilizador(int idUtilizador) {
        this.idUtilizador = idUtilizador;
    }

    /**
     * Getter genérico da variavel nome
     *
     * @return this.nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Setter genérico da variavel nome
     *
     * @param Nome nome a ser guardado
     */
    public void setNome(String Nome) {
        this.nome = Nome;
    }

    /**
     * Getter genérico da variavel idade
     *
     * @return this.idade
     */
    public int getIdade() {
        return idade;
    }

    /**
     * Setter genérico da variavel idade
     *
     * @param Idade idade a ser guardado
     */
    public void setIdade(int Idade) {
        this.idade = Idade;
    }

    /**
     * Getter genérico da variavel sexo
     *
     * @return this.sexo
     */
    public char getSexo() {
        return sexo;
    }

    /**
     * Getter genérico da variavel email
     *
     * @return this.email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter genérico da variavel email
     *
     * @param Email a ser guardado
     */
    public void setEmail(String Email) {
        this.email = Email;
    }

    /**
     * Getter da lista das formações do utilizador 
     * @return this.formacao
     */
    public OrderedArray getFormacaoAcademica() {
        return formacao;
    }

    /**
     * Setter genérico da variavel formacao
     * @param FormacaoAcademica formação a ser guardada 
     */
    public void setFormacaoAcademica(OrderedArray FormacaoAcademica) {
        this.formacao = FormacaoAcademica;
    }
    
    /**
     * Getter da variavel profissional(lista)
     * @return this.empregos
     */
    public OrderedArray getProfissional() {
        return empregos;
    }

    /**
     * Setter da variavel empregos (Lista ordenada)
     * @param Profissional Lista a guardar em empregos
     */
    public void setProfissional(OrderedArray Profissional) {
        this.empregos = Profissional;
    }

    /**
     * Getter genérico da variavel skills
     * @return 
     */
    public UnorderedLinkedList getSkills() {
        return skills;
    }

    /**
     * Setter genérico da variavel skills
     * @param Skills Lista de skills
     */
    public void setSkills(UnorderedLinkedList Skills) {
        this.skills = Skills;
    }

    /**
     * Getter generico da variavel mencoes
     * @return as mençoes que o utilizador tem
     */
    public UnorderedLinkedList getMencoes() {
        return mencoes;
    }

    /**
     * Setter generico da variavel mencoes
     * @param mencoes Lista de menções 
     */
    public void setMencoes(UnorderedLinkedList mencoes) {
        this.mencoes = mencoes;
    }

    /**
     * Getter genérico da variavel contactos
     * @return 
     */
    public UnorderedLinkedList getContactos() {
        return contactos;
    }

    /**
     * Setter genérico da variavel contactos
     * @param contactos lista de contactos
     */
    public void setContactos(UnorderedLinkedList contactos) {
        this.contactos = contactos;
    }

    /**
     * getter genérico da variavel numerovisualizacoes
     * @return numero de visualizacoes do utilizador
     */
    public double getNumeroVisualizacoes() {
        return numeroVisualizacoes;
    }

    /**
     * Setter genérico da variavel numeroVisualizacoes
     * @param numeroVisualizacoes inteiro com o numero de visualizacoes
     */
    public void setNumeroVisualizacoes(double numeroVisualizacoes) {
        this.numeroVisualizacoes = numeroVisualizacoes;
    }
    
    public int getMencoesSize(){
        return this.mencoes.size();
    }
    
    @Override
    public String toString(){
        return "Nome: "+this.nome+"\nIdade: "+this.idade+"\nEmail: "+this.email+"\nNumero de Visualizações: "+this.numeroVisualizacoes;
    }
}
